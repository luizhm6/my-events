import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// import { LoginComponent } from './login/login.component';
import { EventsComponent } from './events/events.component';
import { EventDetailsComponent } from './events/event-details/event-details.component';
import { NotFoundComponent } from './not-found/not-found.component';


const appRoutes: Routes = [
	// { path: 'login',  component: LoginComponent, },
	// { path: 'events/:id',  component: EventsComponent, },
	{ path: 'events/:id',  component: EventDetailsComponent, },
	{ path: 'not-found', component: NotFoundComponent }
]

@NgModule({
	imports: [

		CommonModule,
		RouterModule.forRoot(appRoutes, {
			onSameUrlNavigation: 'reload'
		})
	],
	exports: [
		RouterModule
	],
	declarations: []
})
export class AppRoutingModule { }
