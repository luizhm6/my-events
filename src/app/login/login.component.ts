import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag'
import { AuthService } from '../auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	telefone

	constructor(
		private formBuilder: FormBuilder,
		private apollo: Apollo,
		private authService: AuthService
	) { }

	formLogin: FormGroup = this.formBuilder.group({
		phone: [null, Validators.required],
		password: [null, Validators.required],
	})

	ngOnInit() {
	}

	submit() {
		const url = [
			'auth', 
			'sign_in'
		].join('/')
		this.authService.login(url, this.formLogin.value)
	}
}
