import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Location } from '@angular/common';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	url = environment.url
	constructor(
		private http: HttpClient,
		private _location: Location
	) {
	}

	login(url, data) {
		let headers = new HttpHeaders();
		headers = headers.append('Content-Type', 'application/json');
		headers = headers.append(`Accept`, `application/json`);
		return this.http.post(`http://api.myevents.com.br/api/v1/${url}`, data, { headers: headers })
			.toPromise()
			.then(
				(res: any) => {
					localStorage.setItem('user', JSON.stringify(res));
					this._location.back();
				}
			)
	}
}
