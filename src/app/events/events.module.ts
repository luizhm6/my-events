import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsComponent } from './events.component';
import { MatModule } from '../mat.module';
import { EventDetailsComponent } from './event-details/event-details.component';
import { HeaderComponent } from '../header/header.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ApolloModule, Apollo } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { ApolloLink } from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { onError } from 'apollo-link-error';

import { AgmCoreModule } from '@agm/core';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	suppressScrollX: true
};


@NgModule({
	declarations: [
		EventsComponent,
		EventDetailsComponent,
		HeaderComponent
	],
	imports: [
		CommonModule,
		MatModule,
		RouterModule,
		HttpClientModule,
		ApolloModule,
		HttpLinkModule,
		PerfectScrollbarModule,
		AgmCoreModule.forRoot({
			apiKey: 'AIzaSyCUHgEXdFOSfDFK9AD208A8UJM0q__K-UE'
		})
	],
	providers: [
		{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		}
	],
	exports: [
		EventsComponent
	]
})
export class EventsModule {

}
