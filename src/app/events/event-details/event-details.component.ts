import { Component, OnInit } from '@angular/core';
import { EventsService } from '../events.service';
import { Apollo } from 'apollo-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-event-details',
	templateUrl: './event-details.component.html',
	styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {
	peoples: any;
	id: any;
	event: any;
	eventDocuments: any;
	eventImages: any;

	constructor(
		private eventsService: EventsService,
		private apollo: Apollo,
		private http: HttpClient,
		private route: ActivatedRoute,
	) {
		this.route.params.subscribe(
			params => {
				this.id = params['id']
			}
		);
	}
	lat: number = 0;
	lng: number = 0;

	ngOnInit() {
		this.event = this.eventsService.getEvent(this.id).subscribe(
			res => {
				console.log(res);
				return res;
			},
			error=>{
				console.error(error.status);
				error.status
			}
		);
		this.eventDocuments = this.eventsService.getEventDocuments(this.id).subscribe(
			res => {
				console.log(res);
				return res;
			},
			error=>{
				console.error(error.status);
				error.status
			}
		);
		this.eventImages = this.eventsService.getEventImages(this.id).subscribe(
			res => {
				console.log(res);
				return res;
			},
			error=>{
				console.error(error.status);
				error.status
			}
		);
	}

	redirectToMap() {
		window.open(`https://www.google.com/maps/search/?api=1&query=${this.lat},${this.lng}`, '_blank');
	}

}
