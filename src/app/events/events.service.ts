import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable({
	providedIn: 'root'
})
export class EventsService {

	constructor(
		private http: HttpClient
	) { }

	getEvent(id) {
		return this.http.get(`http://api.myevents.com.br/api/v1/public/event_preview?id_event=${id}`)
			
	}
	getEventDocuments(id) {
		return this.http.get(`http://api.myevents.com.br/api/v1/public/document_preview?id_event=${id}`)
			
	}
	getEventImages(id) {
		return this.http.get(`http://api.myevents.com.br/api/v1/public/image_preview?id_event=${id}`)
		
	}
}
