import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EventsComponent } from './events/events.component';
import { EventsModule } from './events/events.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LoginComponent } from './login/login.component';


import { AppRoutingModule } from './app.routing.module';
import { MatModule } from './mat.module';
import { GraphQLModule } from './graphql.module';
import { ReactiveFormsModule } from '@angular/forms';

import {NgxMaskModule} from 'ngx-mask';
import { NotFoundComponent } from './not-found/not-found.component'

export function HttpLoaderFactory(httpClient: HttpClient) {
	return new TranslateHttpLoader(httpClient);
}

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		NotFoundComponent,
	],
	imports: [
		BrowserModule,
		EventsModule,
		BrowserAnimationsModule,
		HttpClientModule,
		NgxMaskModule.forRoot(),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			}
		}),
		AppRoutingModule,
		MatModule,
		GraphQLModule,
		ReactiveFormsModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
